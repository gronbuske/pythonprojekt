#!/usr/bin/python3
# -*- coding: ISO-8859-1 -*-

import ts3
import urllib.request
import io
import datetime
#from bs4 import BeautifulSoup
import bs4

with ts3.query.TS3Connection("192.168.1.75") as ts3conn:
        # Note, that the client will wait for the response and raise a
        # **TS3QueryError** if the error id of the response is not 0.
        try:
                ts3conn.login(
                        client_login_name="Albert",
                        client_login_password="eHRt4PUf"
                )
        except ts3.query.TS3QueryError as err:
                print("Login failed:", err.resp.error["msg"])
                exit(1)

        ts3conn.use(sid=1)
        now = datetime.datetime.now()
        url = "https://www.citygross.se/mat/matkasse/klassisk-matkasse/?year=" + str(now.year) + "&week=" + str(now.isocalendar()[1] + 1) + "&code=CGMatkasse4p"
        u = urllib.request.urlopen(url, data = None)
        f = io.TextIOWrapper(u,encoding='utf-8')
        text = f.read()

        soup = bs4.BeautifulSoup(text, "html.parser")
        souprows = soup.findAll("div", {"class": "recipe-info"})
        
        weekmenu = ['', '', '', '', '']
        counter = 0
        for div in souprows:
            x = div.h2.contents
            weekmenu[counter] = x[0]
            counter += 1
        

        matsalsID = ts3conn.channelfind(pattern="Matsalen")
        matsalsID = matsalsID[0]["cid"]
        menyUppdaterad = False
        oldDesc = ts3conn.channelinfo(cid=matsalsID)[0]["channel_description"].split('\n')
        newDesc = "Fyll i med era egna menyer l�ngst ner\n\nRestaurang Kungsmarken\nVeckans meny:\n"
        newDesc += "M�ndag: " + str(weekmenu[0]) + "\n"
        newDesc += "Tisdag: " + str(weekmenu[1]) + "\n"
        newDesc += "Onsdag: " + str(weekmenu[2]) + "\n"
        newDesc += "Torsdag: " + str(weekmenu[3]) + "\n"
        newDesc += "Fredag: " + str(weekmenu[4]) + "\n"
        newDesc += "[URL=" + url + "]Recept[/URL]\n"
        nrRows = len(oldDesc)
        for row in range(10, nrRows):
            newDesc += oldDesc[row]
        
        ts3conn.channeledit(cid=matsalsID, channel_description=newDesc)