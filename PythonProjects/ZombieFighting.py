import operator as op
import functools

def ncr(n, r):
    r = min(r, n-r)
    if r == 0: return 1
    numer = functools.reduce(op.mul, range(n, n-r, -1))
    denom = functools.reduce(op.mul, range(1, r+1))
    return numer//denom

if __name__ == '__main__':
    txt = open("zombieinput.txt", 'r')
    lines = txt.readlines()
    txt.close()
    output = []
    

    zombies = int(lines.pop(0))
    for x in range(0, zombies):
        line = lines.pop(0)
        line = line.split(" ")
        health = int(line[0])
        amtSpells = line[1]
        line = lines.pop(0)
        spells = line.split(" ")
        bestChance = 0.0
        for spell in spells:
            spell = spell.strip("\n")
            spell = spell.split("d")
            dice = int(spell.pop(0))
            bonus = 0
            spell = spell[0]
            if len(spell.split("+")) > 1:
                type = int(spell.split("+")[0])
                bonus = int(spell.split("+")[1])
            elif len(spell.split("-")) > 1:
                type = int(spell.split("-")[0])
                bonus -= int(spell.split("-")[1])
            else:
                type = int(spell)
            tot = pow(type, dice)
            negSum = 0
            dmgNeeded = health - bonus
            sum = 0
            if dmgNeeded - dice <= 0:
                sum = 0
            elif dmgNeeded > dice * type:
                sum = tot
            else:
                for y in range(dice, dmgNeeded):
                    partSum = 0
                    for k in range(0, 1 + int((y - dice) / type)):
                        partSum += pow(-1, k) * ncr(dice, k) * ncr(y - type * k - 1, dice - 1)
                    sum += partSum
                    
                    
                        
            chance = (tot - sum - negSum) / tot
            if chance > bestChance:
                bestChance = chance
        output.insert(x, "Case #" + str(x + 1) + ": " + "{:.6f}".format(bestChance)  + "\n")


    
    outFile = open("ZombieOutput.txt", 'w')
    outFile.writelines(output)
    outFile.flush()
    outFile.close()