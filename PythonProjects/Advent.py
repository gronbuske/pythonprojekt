import sys, getopt
from enum import Enum

def day1():
    txt = open("advent1.txt", 'r')
    lines = txt.readlines()
    txt.close()
    input = lines.pop(0)
    
    sum = 0
    othersum = 0
    counter = 0
    length = len(input.strip())
    oppositeside = input.strip()[0 - int(length / 2)]
    previous = input.strip()[-1]
    for number in input:
        counter += 1
        if oppositeside == number:
            othersum += int(number)
        if previous == number:
            sum += int(number)
        previous = number
        oppositeside = input.strip()[0 - int(length / 2) + counter]

    output.append("Day1:\n" + str(sum) + '\n' + str(othersum) + "\n\n")

def day2():
    txt = open("advent2.txt", 'r')
    lines = txt.readlines()
    txt.close()

    sum = 0
    evensum = 0
    for line in lines:
        counter = 0
        numbers = line.split('_')
        max = 0
        min = 100000
        for number in numbers:
            n1 = int(number)
            counter += 1
            if n1 > max:
                max = n1
            if n1 < min:
                min = n1
            for number2 in numbers[counter:]:
                n2 = int(number2)
                if n1 > n2:
                    if n1 % n2 == 0:
                        evensum += n1 / n2
                else:
                    if n2 % n1 == 0:
                        evensum += n2 / n1
        sum += max - min

    output.append("Day2:\n" + str(sum) + "\n" + str(evensum) + "\n\n")
    
def incAndCheck(number, result):
    number += 1
    if number == 265149:
        output.append("Day3:\n" + str(result) + "\n")
    return number

def day3():
    txt = open("advent3.txt", 'r')
    lines = txt.read()
    txt.close()

    goal = int(lines.strip())

    number = 1
    result = 0
    adds = 1
    subs = 0
    while number <= goal:
        for x in range(0, 2):
            for y in range(0, subs):
                result -= 1
                number = incAndCheck(number, result)
            for y in range(0, adds):
                result += 1
                number = incAndCheck(number, result)
        subs += 1
        for x in range(0, 2):
            for y in range(0, subs):
                result -= 1
                number = incAndCheck(number, result)
            for y in range(0, adds):
                result += 1
                number = incAndCheck(number, result)
        adds += 1

    numbers1 = [1, 2, 4, 5, 10, 11, 23, 25]
    curr = 25
    pauses = 2
    while curr < goal:
        numbers2 = []
        for x in range(0, 4):
            i = pauses * x
            for y in range(0, pauses):
                if x == 0:
                    if y == 0:
                        curr += numbers1[0]
                    elif y == 1:
                        curr += numbers1[-1]
                        curr += numbers1[0]
                        curr += numbers1[1]
                    else:
                        for z in range(0, 3):
                            curr += numbers1[y - z]
                else:
                    if y == 0:
                        curr += numbers2[-2]
                    else:
                        curr += numbers1[i + y - 2]
                    curr += numbers1[i + y]
                    curr += numbers1[i + y - 1]
                numbers2.append(curr)
            for y in range(0, 2):
                if x == 3:
                    curr += numbers2[0]
                curr += numbers1[i + pauses - 1]
                if y == 0:
                    curr += numbers1[i + pauses - 2]
                numbers2.append(curr)
        numbers1 = numbers2
        pauses += 2

    for n in numbers1:
        if n > goal:
            output.append(str(n) + "\n\n")
            break

def day4():
    txt = open("advent4.txt", 'r')
    lines = txt.readlines()
    txt.close()

    result = 0
    resultSorted = 0
    for line in lines:
        failed = False
        failedSorted = False
        line = line.strip().split(' ')
        for x in range(len(line)):
            for y in range(x):
                if line[x] == line[y]:
                    failed = True
                if sorted(line[x]) == sorted(line[y]):
                    failedSorted = True
        if not failed:
            result += 1
        if not failedSorted:
            resultSorted += 1

    output.append("Day4:\n" + str(result) + '\n' + str(resultSorted) + "\n\n")

def day5():
    txt = open("advent5.txt", 'r')
    lines = txt.read()
    txt.close()
    lines = lines.split('\n')
    
    lines = list(map(int, lines))
    lines2 = lines

    i = 0
    counter1 = 0
    while i < len(lines):
        temp = lines[i]
        lines[i] += 1
        i += temp
        counter1 += 1

    i = 0
    counter2 = 0
    temp = 0
    while i < len(lines2):
        temp = lines2[i]
        if temp > 2:
            lines2[i] -= 1
        else:
            lines2[i] += 1
        i += temp
        counter2 += 1

    output.append("Day5:\n" + str(counter1) + "\n" + str(counter2) + "\n\n")

def day6():
    txt = open("advent6.txt", 'r')
    lines = txt.read()
    txt.close()
    input = list(map(int, lines.split("\t")))

    formerResults = []
    lastResult = input

    counter1 = 0
    while input not in formerResults:
        formerResults.append(list(input))
        maxNr = max(input)
        for x in range(len(input)):
            if input[x] == maxNr: break
        input[x] = 0
        x += 1
        for y in range(x, x + maxNr):
            input[y % len(input)] += 1
        counter1 += 1
    
    for z in range(len(formerResults)):
        if formerResults[z] == input: break

    output.append("Day6:\n" + str(counter1) + "\n" + str(counter1 - z) + "\n\n")

root = None

class node:
    def __init__(self, weight, name):
        self.name = name
        self.weight = int(weight)
        self.childList = []
        self.parent = None

    def addChild(self, child):
        self.childList.append(child)

    def setParent(self, parent):
        self.parent = parent

def createNode(newNode, parent, knownChild, input):
    t = node(newNode[1], newNode[0])
    if knownChild is not None:
        t.addChild(knownChild)
        knownChild.setParent(t)
    if parent is not None:
        t.setParent(parent)
        parent.addChild(t)
    else:
        index = -1
        for x in range(len(input)):
            if len(input[x]) > 2:
                for y in range(2, len(input[x])):
                    if input[x][y] == newNode[0]:
                        index = x
        if index == -1:
            global root
            root = t
            output.append("Day7:\n" + t.name + "\n")
        else:
            par = input.pop(index)
            createNode(par, None, t, input)

    for i in range(2, len(newNode)):
        if knownChild is not None and newNode[i] == knownChild.name:
            continue
        for j in range(len(input)):
            if input[j][0] == newNode[i]:
                break
        child = input.pop(j)
        createNode(child, t, None, input)

def calcBurden(node):
    weights = []
    for child in node.childList:
        weights.append(calcBurden(child))
    
    if len(weights) == 0:
        return node.weight
    elif len(set(weights)) is not 1:
        oddone = 0
        other = 0
        s = list(set(weights))
        if weights.count(s[0]) > weights.count(s[1]):
            oddone = s[1]
            other = s[0]
        else:
            other = s[1]
            oddone = s[0]
        diff = oddone - other
        output.append(str(node.childList[weights.index(oddone)].weight - diff) + "\n\n")
        return sum(weights) + node.weight - diff
    else:
        return sum(weights) + node.weight

def day7():
    txt = open("advent7.txt", 'r')
    lines = txt.readlines()
    txt.close()
    input = []
    for line in lines:
        line = line.replace(" -> ", " ")
        line = line.replace(",", "")
        line = line.replace("\n", "")
        line = line.replace("(", "")
        line = line.replace(")", "")
        line = line.split(" ")
        input.append(line)

        
    newNode = input.pop(0)
    createNode(newNode, None, None, input)
    
    calcBurden(root)

def day8():
    txt = open("advent8.txt", 'r')
    lines = txt.readlines()
    txt.close()
    varMap = {}
    highestNumber = 0
    for line in lines:
        line = line.replace("\n", "")
        line = line.split(" ")
        varToComp = line[4]
        if varToComp in varMap:
            varToComp = varMap[varToComp]
        else:
            varMap[varToComp] = 0
            varToComp = 0
        compSign = line[5]
        numToComp = int(line[6])
        doChange = False
        if compSign == "<" and varToComp < numToComp:
            doChange = True
        elif compSign == ">" and varToComp > numToComp:
            doChange = True
        elif compSign == "<=" and varToComp <= numToComp:
            doChange = True
        elif compSign == ">=" and varToComp >= numToComp:
            doChange = True
        elif compSign == "==" and varToComp == numToComp:
            doChange = True
        elif compSign == "!=" and varToComp != numToComp:
            doChange = True
        if doChange:
            if line[0] not in varMap:
                varMap[line[0]] = 0
            if line[1] == "inc":
                varMap[line[0]] += int(line[2])
            elif line[1] == "dec":
                varMap[line[0]] -= int(line[2])
            highestNumber = max(highestNumber, varMap[line[0]])

    valueList = list(varMap.items())
    output.append("Day8:\n" + str(max([a[1] for a in valueList])) + "\n" + str(highestNumber) + "\n\n")

def day9():
    txt = open("advent9.txt", 'r')
    lines = txt.read()
    txt.close()

    sum = 0
    removedGarbage = 0
    SkipNext = False
    Garbage = False
    groupNumber = 0
    for c in lines:
        if SkipNext:
            SkipNext = False
        elif c == '!':
            SkipNext = True
        elif Garbage:
            if c == '>':
                Garbage = False
            else:
                removedGarbage += 1
        elif c == '<':
            Garbage = True
        elif c == '{':
            groupNumber += 1
        elif c == '}':
            sum += groupNumber
            groupNumber -= 1

    output.append("Day8:\n" + str(sum) + "\n" + str(removedGarbage) + "\n\n")

def day10():
    txt = open("advent10.txt", 'r')
    lines = txt.read().split(",")
    txt.close()

    circleList = range(0, 256)
    curr = 100
    for i in lines:
        i = int(i)
        listToInvert = [list(range(curr, curr + i)) if curr + i <= len(circleList) else list(range(curr, 256)) + list(range(0, (curr + i) % len(circleList)))]
        invertedList = listToInvert.reverse()


    output.append("Day8:\n" + str(1) + "\n" + str(1) + "\n\n")

if __name__ == '__main__':
    days = ""
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hd:",["days="])
    except getopt.GetoptError:
        print("Advent.py -d <1,2,3 osv.>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("Advent.py -d <1,2,3 osv.>")
            sys.exit()
        elif opt in ("-d", "--days"):
            days = arg
    if days == "":
        runday = [True] * 26
    else:
        runday = [False] * 26
    for i in days.split(","):
        try:
            i = int(i)
            if i > 0 and i < 26:
                runday[i] = True
        except:
            continue

    output = []
    if runday[1]:
        day1()
    if runday[2]:
        day2()
    if runday[3]:
        day3()
    if runday[4]:
        day4()
    if runday[5]:
        day5()
    if runday[6]:
        day6()
    if runday[7]:
        day7()
    if runday[8]:
        day8()
    if runday[9]:
        day9()
    if runday[10]:
        day10()
    

    for line in output: print(line)
    outFile = open("output.txt", 'w')
    outFile.writelines(output)
    outFile.flush()
    outFile.close()