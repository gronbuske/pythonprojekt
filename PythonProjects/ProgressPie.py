import math

if __name__ == '__main__':
    txt = open("pieinput.txt", 'r')
    lines = txt.readlines()
    txt.close()
    output = []
    cases = int(lines.pop(0))
    
    for case in range(0, cases):
        black = False
        line = lines.pop(0)
        line = line.split(" ")
        progress = int(line[0])
        x = line[1]
        y = line[2]
        x = int(x) - 50   
        y = int(y) - 50
        if math.sqrt(pow(x, 2) + pow(y, 2)) > 50:
            black = False
        else:
            if x < 0 and progress > 50:
                if y > 0 and progress > 75:#Upper left
                    angle = math.atan(-y/x)
                    if angle <= (((progress - 75) / 50) * (math.pi)):
                        black = True
                    
                elif y <= 0:#Lower left
                    if y == 0:
                        if progress >= 75:
                            black = True
                    else:
                        angle = math.atan(x/y)
                        if angle <= (((progress - 50) / 50) * (math.pi)):
                            black = True
                    
            elif x >= 0:
                if y < 0 and progress > 25:#Lower right
                    if x == 0:
                        if progress >= 50:
                            black = True
                    else:
                        angle = math.atan(-y/x)
                        if angle <= (((progress - 25) / 50) * (math.pi)):
                            black = True
                    
                elif y >= 0:#Upper right
                    if y == 0:
                        if progress >= 25:
                            black = True
                    else:
                        angle = math.atan(x/y)
                        if angle <= ((progress / 50) * (math.pi)):
                            black = True
        if black:
            output.insert(case, "Case #" + str(case + 1) + ": " + "black\n")
        else:    
            output.insert(case, "Case #" + str(case + 1) + ": " + "white\n")
            
    
    outFile = open("pieoutput.txt", 'w')
    outFile.writelines(output)
    outFile.flush()
    outFile.close()