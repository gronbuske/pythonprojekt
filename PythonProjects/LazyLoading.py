

if __name__ == '__main__':
    txt = open("input.txt", 'r')
    lines = txt.readlines()
    txt.close()
    days = int(lines[0])
    counter = 1
    output = []
    #Loop through days
    for x in range(0, days):
        items = int(lines[counter])
        counter += 1
        weights = []
        #Loop through each day
        for y in range(0, items):
            weights.insert(y, (int(lines[counter])))
            counter += 1
        #Sort weights
        weights.sort()
        #
        trips = 0
        while len(weights) > 0:
            trips += 1
            if len(weights) <= 1:
                break
            largestItem = weights.pop()
            itemsNeeded = int(1 + 50 / largestItem)
            nextNeeded = int(1 + 50 / weights[len(weights) - 1])
            length = len(weights)
            if (itemsNeeded + nextNeeded) <= length + 1:
                for a in range(0, itemsNeeded - 1):
                    weights.pop(0)
            else:
                weights = []
        output.insert(x, "Case #" + str(x + 1) + ": " + str(trips) + "\n")
    outFile = open("output.txt", 'w')
    outFile.writelines(output)
    outFile.flush()
    outFile.close()